import Foundation
import SDWebImage

class AvatarUtils {

    // shared instance means that we dont end up with 1000s of placeholder images floating around in memory
    static let sharedInstance = AvatarUtils()

    private let scale = UIScreen.main.scale
    private let placeholderImage = UIImage(named: "placeholder-avatar")!

    private let sdWebImageOptions: SDWebImageOptions = [
        // stops the library from caching the image beyond what is set in the cache headers
        // we have to do this as group avatars dont have cache busters
        .refreshCached,
        // stops the library from blacklisting failed downloads
        .retryFailed,
        // download images even when app is closed
        .continueInBackground
    ]

    private init() {}

    func configure(_ imageView: UIImageView, group: Group, size: Int) {
        configure(imageView, avatarUrl: group.avatarUrl, size: size)
    }

    func configure(_ imageView: UIImageView, room: Room, size: Int) {
        configure(imageView, avatarUrl: room.avatarUrl, size: size)
    }

    func configure(_ imageView: UIImageView, avatarUrl: String, size: Int) {
        let url = createUrl(avatarUrl, size: size)
        imageView.sd_setImage(with: url, placeholderImage: placeholderImage, options: sdWebImageOptions)
    }

    func createUrl(_ urlString: String, size: Int) -> URL {
        let pixelSize = Int(CGFloat(size) * scale)
        var urlComponents = URLComponents(string: urlString)!
        urlComponents.queryItems = (urlComponents.queryItems ?? []).filter({ (queryItem) -> Bool in
            return queryItem.name != "s"
        }) + [URLQueryItem(name: "s", value: String(pixelSize))]

        return urlComponents.url!
    }
}
