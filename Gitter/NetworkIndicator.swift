import Foundation

class NetworkIndicator {
    static let sharedInstance = NetworkIndicator()

    var visible: Bool = false {
        didSet {
            if visible {
                connections += 1
            } else {
                connections -= 1
            }

            NSLog("Network connection count: %i", connections)
            application.isNetworkActivityIndicatorVisible = connections > 0
        }
    }

    private var connections = 0
    private let application = UIApplication.shared

    private init () {}
}
