import CoreData

class User: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var username: String
    @NSManaged var displayName: String
    @NSManaged var avatarUrl: String
    @NSManaged var oneToOneRoom: Room?
}
