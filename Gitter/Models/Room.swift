import Foundation
import CoreData

class Room: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var oneToOne: Bool
    @NSManaged var uri: String
    @NSManaged var url: String
    @NSManaged var avatarUrl: String
    @NSManaged var topic: String?
    @NSManaged var user_avatarUrlMedium: String?
    @NSManaged var activity: Bool
    @NSManaged var lurk: Bool
    @NSManaged var favourite: Int32
    @NSManaged var favourite_comparable: Int32
    // NSManaged objects only support NSNumber as optionals. No Int or Int32
    @NSManaged var unreadItems: NSNumber?
    @NSManaged var mentions: NSNumber?
    @NSManaged var userCount: Int32
    @NSManaged var roomMember: Bool
    @NSManaged var premium: Bool
    @NSManaged var permissions_admin: Bool
    @NSManaged var lastAccessTime: Date?
    @NSManaged var groupId: String?
    @NSManaged var group: Group?
    @NSManaged var userRoomCollection: UserRoomCollection?
    @NSManaged var userSuggestionCollection: UserSuggestionCollection?
    @NSManaged var user: User?

    var intUnreadItems: Int {
        get {
            return unreadItems?.intValue ?? 0
        }
    }

    var intMentions: Int {
        get {
            return mentions?.intValue ?? 0
        }
    }
}
